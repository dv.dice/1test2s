package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;
import org.w3c.dom.ls.LSOutput;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;
    List<Author> authorList;
    static final String pathAuthors = "src\\itis\\socialtest\\resources\\Authors.csv";
    static final String pathPosts = "src\\itis\\socialtest\\resources\\PostDatabase.scv";

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) throws Exception {
        new MainClass().run(pathPosts, pathAuthors);


    }

    private void run(String postsSourcePath, String authorsSourcePath) throws Exception {
        try (BufferedReader brAuthors = new BufferedReader(new FileReader(authorsSourcePath))) {
            String line;
            while ((line = brAuthors.readLine()) != null) {
                List list = Arrays.asList(line.split(", "));
                authorList = new ArrayList<>();
                authorList.add(deserializationAuth(list));
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try(BufferedReader brPosts = new BufferedReader(new FileReader(authorsSourcePath))) {
            String line;
            while ((line = brPosts.readLine()) != null) {
                List list = Arrays.asList(line.split(", "));
                allPosts.add(deserializationPost(list));
                System.out.println(allPosts);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private Author deserializationAuth(List splittedArray) throws Exception {
        Long id = Long.parseLong((String) splittedArray.get(0));
        String nickname = (String) splittedArray.get(1);
        String birthDay = (String) splittedArray.get(2);
        Author author = new Author(id, nickname, birthDay);
        return author;
    }
    private Post deserializationPost(List splittedArray) throws Exception {
        String date = (String) splittedArray.get(2);
        String content = (String) splittedArray.get(3);
        Long likesCount = Long.parseLong((String) splittedArray.get(1));
        Author author = authorList.get((Integer) splittedArray.get(0) - 1);

        Post post = new Post(date, content,likesCount, author);
        return post;
    }
    public String toString() {
        return allPosts.get(0).toString();
    }
}

